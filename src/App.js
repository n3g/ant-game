import React from 'react'
import './App.scss'
import targets from './components/targets'
import Game from './components/Game'

function App() {
  return (
    <div className='App'>
      <h1>Ant Game</h1>
      <Game targets={targets} />
    </div>
  )
}

export default App

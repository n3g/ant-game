import React from 'react'

const GameInfo = ({ reset, count, randomId, damageDone, score, nbCoups }) => {
  const DisplayDamageDone = () => {
    if (count >= 1 && count < nbCoups) {
      return (
        <p>
          Vous avez touché la fourmi <b>{randomId}</b> lui infligeant{' '}
          <b>{damageDone}</b> dégats !
        </p>
      )
    } else {
      return null
    }
  }

  const DisplayScore = () => {
    if (score !== 0) {
      if (count === 0) {
        return (
          <p>
            <b>SCORE FINAL : {score} !! </b>
          </p>
        )
      } else {
        return (
          <p>
            Score : {score} {score <= 1 ? 'point' : 'points'}
          </p>
        )
      }
    } else {
      return null
    }
  }

  const DisplayCounts = () => {
    if (count !== 0) {
      return (
        <p>
          Il vous reste <b>{count}</b> {count <= 1 ? 'coup' : 'coups'}
        </p>
      )
    } else {
      return null
    }
  }

  if (reset) {
    return null
  } else {
    return (
      <div>
        <DisplayCounts />
        <DisplayDamageDone />
        <DisplayScore />
      </div>
    )
  }
}

export default GameInfo

import React from 'react'

const Buttons = ({ count, reset, handleStart, handleShoot, handleReset }) => {
  if (reset) {
    return (
      <>
        <button className='cta start' onClick={() => handleStart()}>
          Commencer
        </button>
      </>
    )
  } else {
    return (
      <>
        <button className='cta shoot' onClick={() => handleShoot()}>
          {count === 0 ? 'Terminer' : 'Shoot'}
        </button>
        <button className='reset' onClick={() => handleReset()}>
          Recommencer
        </button>
      </>
    )
  }
}

export default Buttons

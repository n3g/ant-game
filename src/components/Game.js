import React, { useState } from 'react'
import AntCards from './AntCards'
import Score from './Score'
import Buttons from './Buttons'
import GameInfo from './GameInfo'

const Game = ({ targets }) => {
  // Deep clone
  const deepClone = targets.map((a) => ({ ...a }))
  const nbCoups = 75

  const [reset, setReset] = useState(true)
  const [targetsCopy, setTargetsCopy] = useState(() => {
    return deepClone
  })
  const [count, setCount] = useState(nbCoups)
  const [randomId, setRandomId] = useState(0)
  const [damageDone, setDamageDone] = useState(0)
  const [score, setScore] = useState(0)
  const [scoreArray, setScoreArray] = useState([])
  const [playerName, setPlayerName] = useState('')

  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max))
  }

  const handleShoot = () => {
    if (count > 0) {
      const rand = getRandomInt(targetsCopy.length)
      const randTarget = targetsCopy[rand]
      const damageDone = randTarget.damage_taken

      // Si la cible est vivante
      if (randTarget.dead === false) {
        randTarget.hp -= damageDone

        // Si la cible meurt
        if (randTarget.hp <= 0) {
          randTarget.hp = 0
          randTarget.dead = true

          // Et si c'est la reine
          if (randTarget.type === 'reine') {
            updateScore()
            handleReset()
            alert('Bravo ! Vous avez tué la reine !')
            return
          }
        }

        // update state
        setRandomId(rand)
        setDamageDone(damageDone)
        setCount(count - 1)
        setScore(score + damageDone * count)
      }
    } else {
      updateScore()
      handleReset()
    }
  }

  const updateScore = () => {
    scoreArray.push([playerName, score])
    setScoreArray(scoreArray)
    handleReset()
  }

  const handleReset = () => {
    setCount(nbCoups)
    setScore(0)
    setRandomId(0)
    setDamageDone(0)
    setTargetsCopy(deepClone)
    setReset(true)
  }

  const handleStart = () => {
    setReset(false)
    if (playerName) {
      if (
        window.confirm(
          'Voulez vous continuer à jouer en tant que : ' + playerName + ' ?'
        )
      ) {
        return
      } else {
        setPlayerName(prompt('Quel est votre nom ?'))
      }
    } else {
      setPlayerName(prompt('Quel est votre nom ?'))
    }
  }

  const Render = () => {
    return (
      <>
        <div className='ants-wrapper'>
          <AntCards ants={targetsCopy} />
          <Buttons
            count={count}
            reset={reset}
            handleStart={handleStart}
            handleReset={handleReset}
            handleShoot={handleShoot}
          />
          <GameInfo
            reset={reset}
            count={count}
            randomId={randomId}
            damageDone={damageDone}
            score={score}
            nbCoups={nbCoups}
          />
        </div>
        <div>
          <Score scores={scoreArray} />
        </div>
      </>
    )
  }

  return <Render />
}

export default Game

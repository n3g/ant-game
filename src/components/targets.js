var targets = [
  {
    type: 'reine',
    hp: 100,
    damage_taken: 8,
    dead: false,
  },
  {
    type: 'soldat',
    hp: 75,
    damage_taken: 10,
    dead: false,
  },
  {
    type: 'travailleuse',
    hp: 50,
    damage_taken: 12,
    dead: false,
  },
]

// Alias
const soldat = targets[1]
const travailleuse = targets[2]

// Ants number
const nbSoldat = 5
const nbTravailleuse = 8

// Populate
for (var i = 1; i < nbTravailleuse; i++) {
  targets.push(soldat)
}

for (var j = 1; j < nbSoldat; j++) {
  targets.push(travailleuse)
}

// Randomize order (for design)
function randomize(a, b) {
  return 0.5 - Math.random()
}

targets = targets.sort(randomize)

export default targets

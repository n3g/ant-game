import React from 'react'

const AntCards = ({ ants }) => {
  return (
    <div>
      <div className='ants-wrapper'>
        {ants.map((target, key) => (
          <div
            className={
              'ant ' + target.type + ' ' + (target.dead ? 'dead' : 'alive')
            }
            key={key}
          >
            <span className='type'>
              {key} - {target.type}
            </span>
            <span className='hp'>{target.hp} HP</span>
            <span className='status'>{target.dead ? 'Morte' : 'En vie'}</span>
          </div>
        ))}
      </div>
    </div>
  )
}

export default AntCards

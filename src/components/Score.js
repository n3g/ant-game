import React from 'react'

const Score = ({ scores }) => {
  if (scores.length > 0) {
    return (
      <>
        <h2>Meilleurs scores</h2>
        <table className='score-table'>
          <tbody>
            {scores.map((score, key) => (
              <tr key={key}>
                <td>
                  <b>{score[0]}</b>
                </td>
                <td>{score[1]} pts</td>
              </tr>
            ))}
          </tbody>
        </table>
      </>
    )
  } else {
    return null
  }
}

export default Score
